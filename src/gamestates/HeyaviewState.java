package gamestates;


import main.GUI;
import main.SumoManager;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import entities.Heya;
import entities.Rikishi;

public class HeyaviewState extends BasicGameState {

	int stateID = -1;
	Image background;
	Heya home;
	String day;
	String time;
	GUI theGUI;
	
	public HeyaviewState(int stateID) throws SlickException {
		this.stateID = stateID;
		background = new Image("heya.jpg");
	}
	
	public void enter(GameContainer container, StateBasedGame game) {
		
		System.out.println(home.getStats());
		
		day = ((GameplayState)game.getState(SumoManager.GAMEPLAYSTATE)).getDay();
		time = ((GameplayState)game.getState(SumoManager.GAMEPLAYSTATE)).getTime();
		
	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {

		theGUI = new GUI(game);
		home = ((GameplayState) game.getState(SumoManager.GAMEPLAYSTATE)).getHome();
		day = ((GameplayState)game.getState(SumoManager.GAMEPLAYSTATE)).getDay();
		time = ((GameplayState)game.getState(SumoManager.GAMEPLAYSTATE)).getTime();
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {

		background.draw(0, 0);
		
		// Draw the main GUI
		theGUI.draw();
		
		// Draw state specific fields
		// Rikishi Stats
		g.drawRect(10, SumoManager.SCREEN_HEIGHT - 75, 200, 50);
		g.drawString("Rikishi Stats", 20, SumoManager.SCREEN_HEIGHT - 60);
		
		// Hire new rikishi field
		g.drawRect(220, SumoManager.SCREEN_HEIGHT - 75, 200, 50);
		g.drawString("Hire new Rikishi", 230, SumoManager.SCREEN_HEIGHT - 60);
		
		// Expand heya field
		g.drawRect(430, SumoManager.SCREEN_HEIGHT - 75, 200, 50);
		g.drawString("Expand Heya", 440, SumoManager.SCREEN_HEIGHT - 60);
		
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {

		Input input = container.getInput();
		
		if(input.isMousePressed(0)) {
			
			
			// Clicked home field
			if(input.getAbsoluteMouseX() >= 10 &&
					input.getAbsoluteMouseX() <= 210 &&
					input.getAbsoluteMouseY() >= 10 &&
					input.getAbsoluteMouseY() <= 60) {
				
				game.enterState(SumoManager.GAMEPLAYSTATE);

			}
			
			// Clicked stats field
			if(input.getAbsoluteMouseX() >= 220 &&
					input.getAbsoluteMouseX() <= 420 &&
					input.getAbsoluteMouseY() >= 10 &&
					input.getAbsoluteMouseY() <= 60) {
				
				game.enterState(SumoManager.HEYAVIEWSTATE);

			}
			
			// Clicked rikishi stats field
			if(input.getAbsoluteMouseX() >= 10 && 
					input.getAbsoluteMouseX() <= 210 &&
					input.getAbsoluteMouseY() >= (SumoManager.SCREEN_HEIGHT - 75) &&
					input.getAbsoluteMouseY() <= (SumoManager.SCREEN_HEIGHT - 25) ) {
						
				for(int i=0; i<home.getRikishi().size(); i++) {
					
					System.out.println(home.getRikishi().get(i).printStats());					
					
				}
			}
			
			// Clicked hire new rikishi field
			if(input.getAbsoluteMouseX() >= 220 && 
					input.getAbsoluteMouseX() <= 420 &&
					input.getAbsoluteMouseY() >= (SumoManager.SCREEN_HEIGHT - 75) &&
					input.getAbsoluteMouseY() <= (SumoManager.SCREEN_HEIGHT - 25) ) {
				
				hireNewRikishi();
				
			}
			
			// Clicked expand heya field
			if(input.getAbsoluteMouseX() >= 430 && 
					input.getAbsoluteMouseX() <= 630 &&
					input.getAbsoluteMouseY() >= (SumoManager.SCREEN_HEIGHT - 75) &&
					input.getAbsoluteMouseY() <= (SumoManager.SCREEN_HEIGHT - 25) ) {
							
				expandHeya();
							
			}
		}
		
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return this.stateID;
	}
	
	/**
	 * Adds a new rikishi to the heya, providing the player has enough space and
	 * money.
	 */
	private void hireNewRikishi() {
		
		if(home.getCap() - home.getRikishi().size() == 0)
			System.out.println("You don't have space for more rikishi!");
		else if(home.getMoney() < 500)
			System.out.println("You don't have enough money for more rikishi!");
		else {
			
			home.decMoneyBy(500);
			Rikishi newbie = new Rikishi(home);
			home.addRikishi(newbie);
			
			System.out.println("Hired "+newbie.getName()+"!");
			// TODO: Assign a rank
		}
		
	}
	
	/**
	 * Builds new rooms to fit in more rikishi
	 */
	private void expandHeya() {
		
		if(home.getMoney() < 1000)
			System.out.println("You don't have enough money for expansion!");
		else {
			home.decMoneyBy(1000);
			home.incCapacity();
			
			System.out.println("You now have space for 1 more rikishi!");
		}
	}

}
