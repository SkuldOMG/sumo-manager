package gamestates;

import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import javax.print.attribute.standard.MediaSize.Other;

import main.*;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import entities.*;


/**
 * The gameplay state. Gameplay takes place here. Duh.
 * 
 * @author Simon Dehne
 *
 */
public class GameplayState extends BasicGameState {
	
	// Game state ID
	int stateID = -1;
	Image background;
	Heya home;
	String time, day;
	GUI theGUI;
	
	// Rikishi not belonging to the home heya
	Vector<Rikishi> otherRikishi;
	// Is a basho underway?
	boolean bashoTime;
	Basho bash;
	
	public GameplayState(int stateID) {
		this.stateID = stateID;
	}
	
	// Initialization method
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		
		// Options for the game
		gc.setShowFPS(false);	
		
		// Image loading
		background = new Image("bg.jpg");
		
		// Enter username
		String username = "Ootori";
		home = new Heya(username+"-beya");
		
		// Variable initialization
		time = "05:00 AM";
		day = "1";
		bashoTime = false;
		
		// GUI Initialization
		theGUI = new GUI(sbg);
		
		System.out.println("Welcome to the world of sumo, "+ username+"-oyakata! " +
				"Your stable is now called "+home.getName()+"!\n");
		
		// Pre-populate the game
		// Player starts with two wrestlers at the lowest possible rank
		home.addRikishi(new Rikishi(home));
		home.addRikishi(new Rikishi(home));
		home.getRikishi().get(0).setRank("JK15-E");
		home.getRikishi().get(1).setRank("JK15-W");
		
		otherRikishi = new Vector<Rikishi>();
		
		// For now, only populate Jonokuchi division
		for(int i=0; i<28; i++) {
			otherRikishi.add(new Rikishi(null));
		}
		
		// TODO: Find a way to make this shit easier
		otherRikishi.get(0).setRank("JK14-E");
		otherRikishi.get(1).setRank("JK14-W");
		otherRikishi.get(2).setRank("JK13-E");
		otherRikishi.get(3).setRank("JK13-W");
		otherRikishi.get(4).setRank("JK12-E");
		otherRikishi.get(5).setRank("JK12-W");
		otherRikishi.get(6).setRank("JK11-E");
		otherRikishi.get(7).setRank("JK11-W");
		otherRikishi.get(8).setRank("JK10-E");
		otherRikishi.get(9).setRank("JK10-W");
		otherRikishi.get(10).setRank("JK09-E");
		otherRikishi.get(11).setRank("JK09-W");
		otherRikishi.get(12).setRank("JK08-E");
		otherRikishi.get(13).setRank("JK08-W");
		otherRikishi.get(14).setRank("JK07-E");
		otherRikishi.get(15).setRank("JK07-W");
		otherRikishi.get(16).setRank("JK06-E");
		otherRikishi.get(17).setRank("JK06-W");
		otherRikishi.get(18).setRank("JK05-E");
		otherRikishi.get(19).setRank("JK05-W");
		otherRikishi.get(20).setRank("JK04-E");
		otherRikishi.get(21).setRank("JK04-W");
		otherRikishi.get(22).setRank("JK03-E");
		otherRikishi.get(23).setRank("JK03-W");
		otherRikishi.get(24).setRank("JK02-E");
		otherRikishi.get(25).setRank("JK02-W");
		otherRikishi.get(26).setRank("JK01-E");
		otherRikishi.get(27).setRank("JK01-W");
		
		/*Rikishi hiyo = new Rikishi();
		Rikishi aka = new Rikishi();
		Rikishi tori = new Rikishi();
		Rikishi anko = new Rikishi();
		Rikishi soppu = new Rikishi();
		Rikishi megane = new Rikishi();
		
		home.addRikishi(hiyo);
		home.addRikishi(aka);
		
		hiyo.setRank("JK01-E");
		aka.setRank("JK03-W");
		tori.setRank("JK05-E");
		anko.setRank("JN15-W");
		soppu.setRank("JK07-E");
		megane.setRank("JK10-W");
		
		Vector<Rikishi> basho = new Vector<Rikishi>();
		basho.add(hiyo);
		basho.add(aka);
		basho.add(tori);
		basho.add(anko);
		basho.add(soppu);
		basho.add(megane);
		
		Basho bash = new Basho(basho);*/
	}
	
	// Update method (input etc. goes here)
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
		Input input = gc.getInput();		

	
		// handle state specific input here
		if(input.isMousePressed(0)) {			
						
			
			// Clicked home field
			if(input.getAbsoluteMouseX() >= 10 &&
					input.getAbsoluteMouseX() <= 210 &&
					input.getAbsoluteMouseY() >= 10 &&
					input.getAbsoluteMouseY() <= 60) {
				
				sbg.enterState(SumoManager.GAMEPLAYSTATE);

			}
			
			// Clicked stats field
			if(input.getAbsoluteMouseX() >= 220 &&
					input.getAbsoluteMouseX() <= 420 &&
					input.getAbsoluteMouseY() >= 10 &&
					input.getAbsoluteMouseY() <= 60) {
				
				if(time.equals("05:00 AM") || time.equals("07:00 AM")) {
					System.out.println("You can't leave the rikishi unattended!");
				}
				else if(bashoTime) {
					// During a basho, print your rikishi's current score instead of
					// going into heya view
					for(int i=0; i < home.getRikishi().size(); i++) {
						
						System.out.println(home.getRikishi().get(i).getName() + 
								": "+home.getRikishi().get(i).getWins()+"-"+
								home.getRikishi().get(i).getLosses());
					}
				}
				else
					sbg.enterState(SumoManager.HEYAVIEWSTATE);

			}
			
			// Clicked timestep field
			if(input.getAbsoluteMouseX() >= 400 && 
					input.getAbsoluteMouseX() <= 600 &&
					input.getAbsoluteMouseY() >= (SumoManager.SCREEN_HEIGHT - 75) &&
					input.getAbsoluteMouseY() <= (SumoManager.SCREEN_HEIGHT - 25) ) {
				
				timestep();
			}			
		}
	}
	
	// Render
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		background.draw(0, 0);
		
		// Draw the main GUI
		theGUI.draw();
		
		// Draw state specific fields
		// Timestep field
		g.drawRect(400, SumoManager.SCREEN_HEIGHT - 75, 200, 50);
		g.drawString("Timestep", 450, SumoManager.SCREEN_HEIGHT - 60);		
	}

	@Override
	public int getID() {
		return this.stateID;
	}
	
	/**
	 * Cycles throughout the day. Different events are tied to different
	 * times, like training, eating, management or mini-games.
	 */
	private void timestep() {
		
		// TODO: Handle income
		
		// First check if it's time for a basho
		// Bashos start every 15 days, so the first one starts on day 16, then 46, 76, 106, ...
		// So we use the formula (day - 16) % 30 == 0 to check if it's time
		if(((Integer.parseInt(day) - 16) % 30 == 0) && time.equals("05:00 AM")) {
						
			bashoTime = true;
			time = "09:00 AM";
			System.out.println("A new basho has started!");
			
			// Add all rikishi to the basho
			Vector<Rikishi> participants = new Vector<Rikishi>();
			participants.addAll(home.getRikishi());
			participants.addAll(otherRikishi);
			
			bash = new Basho(participants);
		}
		
		// If we're in a basho, don't go by the normal daily routine but use the basho routine
		if(bashoTime)
			bashoTimestep();
		
		// If not, just go by the normal routine
		// Training round 1
		else if(time.equals("05:00 AM")) {					
			lowTraining();
		}
		
		// Training round 2
		else if(time.equals("07:00 AM")) {
			highTraining();			
		}
			
		// Bath time
		else if(time.equals("10:30 AM")) {
			bathTime();
		}
		
		// Breakfast time
		else if(time.equals("11:00 AM")) {
			breakfast();
		}
		
		// Nap time
		else if(time.equals("12:00 AM")) { 
			afternoon();
		}
		
		// Dinner time
		else if(time.equals("06:00 PM")) {
			dinner();
		}
		
		// Free time
		else if(time.equals("07:00 PM")) {
			evening();
		}
		
	}
	
	/**
	 * Timeschedule for a tournament.
	 */
	private void bashoTimestep() {
		
		if(time.equals("09:00 AM")) {
			
			// If it's the 15th day, find the winner
			if(bash.getDay() == 15) {
				
				System.out.println("Dawn of the final day!");
				bash.startDay();
				
				bash.calcResults();
				bashoTime = false;
				day = String.valueOf(Integer.parseInt(day) + 1);
				time = "05:00 AM";
			}
			else {
				System.out.println("Day "+bash.getDay()+"'s fights have started!");
				bash.startDay();
				time = "05:00 PM";
			}			
		}
		
		else if(time.equals("05:00 PM")) {
			
			System.out.println("After hours ~");
			day = String.valueOf(Integer.parseInt(day) + 1);
			time = "09:00 AM";
		}
		
		
	}
	
	// Increase random stats for low ranked rikishi
	private void lowTraining() {
		
		System.out.println("Ohio~ Training lower ranked rikishis...");
		
		// For now, train any rikishi regardless of rank, because there is no rank yet, derp
		Vector<Rikishi> rikishi = home.getRikishi();
		
		// First, choose a random number of rikishi whose attributes increase
		Random random = new Random();
		
		// Formula for getting a random number between [max, min]:
		// .nextInt(max - min + 1) + min
		// We want to increase at least 0 rikishi's attributes, up to all rikishi
		int noOfIncreases = random.nextInt(rikishi.size() - 0 + 1) + 0;
		
		// now choose which rikishi to increase
		Vector<Rikishi> increasingRikishi = new Vector<Rikishi>();
		
		// shuffle the vector containing all rikishi and choose [noOfIncreases] rikishi out of it
		Collections.shuffle(rikishi);
		
		for(int i=0; i<noOfIncreases; i++) {
			increasingRikishi.add(rikishi.get(i));
		}
		
		// now we know which rikishi to increase attributes of
		// Choose which attribute to increase
		// Precondition: Normal training only increases one random attribute, not several
				
		for(int i=0; i<increasingRikishi.size(); i++) {
			
			// Choose a random number between 1 and 4
			random = new Random();
			int attribute = random.nextInt(4 - 1 + 1) + 1;
			
			// Increase the corresponding attribute by 1
			switch (attribute) {
				case 1:
				increasingRikishi.get(i).incSpeed();
				System.out.println(increasingRikishi.get(i).getName()+"'s Speed has increased!\n");
				break;
				
				case 2:
				increasingRikishi.get(i).incStamina();
				System.out.println(increasingRikishi.get(i).getName()+"'s Stamina has increased!\n");
				break;
				
				case 3:
				increasingRikishi.get(i).incStrength();
				System.out.println(increasingRikishi.get(i).getName()+"'s Strength has increased!\n");
				break;
				
				case 4:
				increasingRikishi.get(i).incTech();
				System.out.println(increasingRikishi.get(i).getName()+"'s Technique has increased!\n");
				break;			

				default:
				break;
			}			
		}
		
		
		time = "07:00 AM";
	}
	
	// Increase random stats for high ranked rikishi
	// Rarely, stats of lower ranked rikishi may decrease
	// due to seniors overdoing it
	private void highTraining() {
		
		// TODO: Add this
		System.out.println("Training higher ranked rikishis...");
		time = "10:30 AM"; 
	}
	
	// Think of something :3
	private void bathTime() {
		
		System.out.println("Bath time!");
		time = "11:00 AM";
	}
	
	// HP may increase
	private void breakfast() {
		
		System.out.println("Eating breakfast...");
		time = "12:00 AM";
	}
	
	// Maybe time for management activities or something?
	// Like invite other rikishi for training matches etc.
	private void afternoon() {
		
		System.out.println("Taking a nap...");
		time = "06:00 PM";
	}
	
	// HP may increase
	private void dinner() {
		
		System.out.println("Eating dinner...");
		time = "07:00 PM";
	}
	
	// Maybe time for mini-games like going out on a date
	// Or shit like that
	private void evening() {
		
		System.out.println("It's evening~");
		time = "05:00 AM";
		
		// Move a day forward
		day = String.valueOf(Integer.parseInt(day) + 1);
	}

	public Heya getHome() {
		return home;
	}

	public String getTime() {
		return time;
	}

	public String getDay() {
		return day;
	}		

}
