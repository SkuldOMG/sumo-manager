package main;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;

import entities.Heya;
import gamestates.*;

public class GUI  {
	
	StateBasedGame game;
	Graphics g;
	Heya home;
	String time, day;	
	
	/**
	 * Initializing happens. Graphics are obviously needed for drawing the GUI itself.
	 * We also need a StateBasedGame object for accessing data that is displayed in the GUI itself.
	 * @param game
	 */
	public GUI(StateBasedGame game) {
		g = SumoManager.app.getGraphics();
		this.game = game;
		home = ((GameplayState)game.getState(SumoManager.GAMEPLAYSTATE)).getHome();
	}
	
	
	/**
	 * Draw is called in the respective gamestate's draw method and draws the game's
	 * GUI parts that don't change for any gamestate screen.
	 */
	public void draw() {
		
		// Update the gameplay data
		time = ((GameplayState)game.getState(SumoManager.GAMEPLAYSTATE)).getTime();
		day = ((GameplayState)game.getState(SumoManager.GAMEPLAYSTATE)).getDay();
		
		// Draw the GUI
		// Main window field
		g.drawRect(10, 10, 200, 50);
		g.drawString("Main Window", 40, 25);
		
		// Stats field
		g.drawRect(220, 10, 200, 50);
		g.drawString(home.getName()+" Overview", 240, 25);
		
		// Day
		g.drawString("Day "+day, SumoManager.SCREEN_WIDTH - 100, SumoManager.SCREEN_HEIGHT - 50);
		// Time of day
		g.drawString(time, SumoManager.SCREEN_WIDTH - 100, SumoManager.SCREEN_HEIGHT - 25);
		// Money
		g.drawString("Finances: \n"+home.getMoney(), SumoManager.SCREEN_WIDTH - 200, SumoManager.SCREEN_HEIGHT - 50);
		
	}

}
