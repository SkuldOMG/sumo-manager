package main;
import gamestates.*;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class SumoManager extends StateBasedGame {
	
	// The gameplay states
	public static final int MAINMENUSTATE = 0;
	public static final int GAMEPLAYSTATE = 1;
	public static final int HEYAVIEWSTATE = 2;
	
	// Screen width and height
	public static final int SCREEN_WIDTH = 960;
	public static final int SCREEN_HEIGHT = 720;
	
	static AppGameContainer app;
	
	// Constructor for the game
	public SumoManager() {
		super("Sumo Manager");
	}
	
	public static void main(String[] args) throws SlickException {
		
		// Create the game container
		app = new AppGameContainer(new SumoManager());

		app.setDisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT, false);
		app.setTargetFrameRate(120);
		app.start();
	}
	
	// Initialize list of gameplay states 
	public void initStatesList(GameContainer gameContainer) throws SlickException {
		
		// later: this.addState(new MainMenuState(MAINMENUSTATE));
		this.addState(new GameplayState(GAMEPLAYSTATE));
		this.addState(new HeyaviewState(HEYAVIEWSTATE));		
	}
}
