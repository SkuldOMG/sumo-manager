package entities;
import gamestates.GameplayState;

import java.awt.List;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;


public class Rikishi {
	
	private String name;
	private String rank;
	private int strength;
	private int stamina;
	private int speed;
	private int tech;
	private int maxhp, hp;
	// Used for tournaments
	private int wins, losses;
	private Heya homeHeya;
	
	public Rikishi(Heya homeHeya) {
		
		this.name = generateName();		
		
		strength = calcRandomStat(2, 10);
		stamina = calcRandomStat(2, 10);
		speed = calcRandomStat(2, 10);
		tech = calcRandomStat(2, 10);
		maxhp = hp = calcRandomStat(10, 20);
		wins = losses = 0;
		this.homeHeya = homeHeya;
	}
	
	/**
	 * Calculates a random stat value in the range of
	 * min and max
	 * 
	 * @param min	Lower limit
	 * @param max	Upper limit
	 * @return		A random value between the lower and upper limit
	 */
	private int calcRandomStat(int min, int max) {
	
		// Assign random values to the rikishi's attributes
		Random rand = new Random();
		
		// nextInt is exclusive of the top value, adding 1
		// makes it inclusive
		return rand.nextInt(max - min + 1) + min;
		
	}
	
	public void fight(Rikishi opponent) {
		
		boolean firstTurn = true;
		int turnNumber = 0;
		
		while(this.hp > 0 || opponent.hp > 0) {			
			
			// If my speed is higher, start the match, afterwards
			// take turns
			if(this.speed > opponent.getSpeed() && firstTurn) {
				
				turn(this, opponent);				
				if(opponent.getHP() <= 0)
					break;
				
				firstTurn = false;
			}
			
			turn(opponent, this);
			if(this.hp <= 0)
				break;
			
			turn(this, opponent);
			if(opponent.getHP() <= 0)
				break;
			
			turnNumber++;
			
			// If this ends in a draw (might be if both hit for 0 damage) make the opponent lose
			// TODO: Make it random who loses
			if(turnNumber >= 100)
				opponent.setHP(0);
				
		}
		
		if(this.hp <= 0) {
			System.out.println(opponent.getName()+" wins against "+this.getName()+"!\n");
			opponent.incWins();
			this.incLosses();
		}
		else if(opponent.getHP() <= 0) {
			System.out.println(this.getName()+" wins against "+opponent.getName()+"!\n");
			this.incWins();
			opponent.incLosses();
		}
		
		// Reset current hp
		this.hp = maxhp;
		opponent.setHP(opponent.getMaxHP());
	
	}
	
	// TODO: Make hits more random
	// Also skills, avoiding hits completely, kimarite
	private void turn(Rikishi attacker, Rikishi defender) {
		// Go simple for now
		// Formula: Amount of HP hit = Strength - 0.5 * Defender's Stamina
		// Casting to int drops any decimal (ie. rounds down), which is ok for now
		int hit = (int)(attacker.getStrength() - 0.5 * defender.getStamina());
		
		// Make sure not to hit for negative damage
		if(hit < 0)
			hit = 0;
		
		defender.decHPby(hit);
	}
	
	/**
	 * Choose a random prefix and a random suffix from a list, put them together
	 * and voil�, you've got a name.
	 * 
	 * @return The new rikishi's name
	 */
	private String generateName() {
		
		String name = "";
		int prefixLineCount = 0;
		int suffixLineCount = 0;		
		
		try {
			// Load a prefix and a suffix file
			FileReader prefixReader = new FileReader("prefixes");
			FileReader suffixReader = new FileReader("suffixes");
			BufferedReader bufRead = new BufferedReader(prefixReader);			

			String line = bufRead.readLine();			
			
			// First up is the prefix file
			// First, count the number of lines in the file
			while(line != null) {					
			
				prefixLineCount++;
				line = bufRead.readLine();
			}
			
			// Decrease the count because counting starts at 0
			prefixLineCount--;		
			
			prefixReader.close();
			bufRead.close();
			
			// Now for the suffixes
			bufRead = new BufferedReader(suffixReader);
			
			line = bufRead.readLine();
			
			// Count number of lines
			while(line != null) {
				
				suffixLineCount++;
				line = bufRead.readLine();
			}
			
			// Decrease the count because counting starts at 0
			suffixLineCount--;
			
			suffixReader.close();
			bufRead.close();
			
		}
		catch (FileNotFoundException e) {
			
		} catch (IOException e) {

		}		
		
		
		try {
			// Load the files again
			FileReader prefixReader = new FileReader("prefixes");
			FileReader suffixReader = new FileReader("suffixes");
			BufferedReader bufRead = new BufferedReader(prefixReader);
			
			String line = bufRead.readLine();
			
			// First the prefixes
			// Now choose a random line in the file to be used as a prefix
			Random random = new Random();
			int prefLine = random.nextInt(prefixLineCount - 0 + 1) + 0;				
				
			int i = 0;
			while(line != null) {
						
				if(i == prefLine)
					name += line;		
					
				i++;
				line = bufRead.readLine();
			}
			
			prefixReader.close();
			bufRead.close();
			
			// Every third name, slap a "no" in there
			Random rand = new Random();
			int no = rand.nextInt(100 - 1 + 1) + 1;
			
			if(no % 3 == 0) {
				name += "no";
			}
			
			// Now for the suffixes
			bufRead = new BufferedReader(suffixReader);
			
			line = bufRead.readLine();
			
			// Choose random line in the file
			random = new Random();
			int sufLine = random.nextInt(suffixLineCount - 0 + 1) + 0;
			
			i = 0;
			
			// Search for the line and add the suffix to the name
			while(line != null) {			
			
				if(i == sufLine)
					name += line;
				
				i++;
				line = bufRead.readLine();
			}
			
			suffixReader.close();
			bufRead.close();
		}
		catch (FileNotFoundException e) {
			
		} catch (IOException e) {

		}
		
		return name;
	}
	
	// ------------- GETTERS --------------
	
	public String printStats() {
		
		return "Name: "+name+"\nRank: "+rank+"\nHP: "+hp+"\nStrength: "+strength+"\nStamina: "+stamina+"\nSpeed: "+speed+
				"\nTech: "+tech+"\n";
	}	
	
	public String getName() {
		return name;
	}
	
	public int getHP() {
		return hp;
	}
	
	public int getStrength() {
		return strength;
	}
	
	public int getStamina() {
		return stamina;
	}
	
	public int getSpeed() {
		return speed;
	}
	
	public int getTech() {
		return tech;
	}
	
	public int getMaxHP() {
		return maxhp;
	}
	
	public int getWins() {
		return wins;
	}
	
	public int getLosses() {
		return losses;
	}
	
	public String getRank() {
		return rank;
	}
	
	// ----------- SETTERS -----------------	
	public void incStrength() {
		strength++;
	}
	
	public void decStrength() {
		strength--;
	}
	
	public void incHP() {
		hp++;
	}
	
	public void decHP() {
		hp--;
	}
	
	public void setHP(int hp) {
		this.hp = hp;
	}
	
	public void decHPby(int val) {
		hp -= val;
	}
	
	public void incStamina() {
		stamina++;
	}
	
	public void decStamina() {
		stamina--;
	}
	
	public void incSpeed() {
		speed++;
	}
	
	public void decSpeed() {
		speed--;
	}
	
	public void incTech() {
		tech++;
	}
	
	public void decTech() {
		tech--;
	}
	
	public void incMaxHP() {
		maxhp++;
	}
	
	public void decMaxHp() {
		maxhp--;
	}
	
	public void incWins() {
		wins++;
	}
	
	public void incLosses() {
		losses++;
	}
	
	public void setWins(int wins) {
		this.wins = wins;
	}
	
	public void setLosses(int losses) {
		this.losses = losses;
	}
	

	/**
	 * Splits a string containing a rank into three separate strings.
	 * 
	 * Valid strings for rank look like this: XY12-E
	 * Where XY is the code for the division, followed by two numbers
	 * Indicating the rank, followed by either -E or -W to denote east or west
	 * 
	 * @param rank				The complete rank string that is to be split
	 * @return	splitString		A vector object containing division, position and side
	 */
	private Vector<String> splitRankString(String rank) {
		
		Vector<String> splitString = new Vector<String>();		

		String division = "";
		String pos = "";
		String side = "";
		
		// Split the rank String into division, position and east/west side
		
		// Depending on whether we have a leading 0 or a leading 1, different substrings have
		// to be used
		if(rank.indexOf("0") > 0 && rank.indexOf("0") < rank.indexOf("1")) {
			division = rank.substring(0, rank.indexOf("0"));
			pos = rank.substring(rank.indexOf("0"), rank.indexOf("0")+2);
		}
		else if(rank.indexOf("1") > 0 && rank.indexOf("1") < rank.indexOf("0")) {
			division = rank.substring(0, rank.indexOf("1"));
			pos = rank.substring(rank.indexOf("1"), rank.indexOf("1")+2);
		}
		else if(rank.indexOf("0") < 0) {
			division = rank.substring(0, rank.indexOf("1"));
			pos = rank.substring(rank.indexOf("1"), rank.indexOf("1")+2);
		}
		else if(rank.indexOf("1") < 0) {
			division = rank.substring(0, rank.indexOf("0"));
			pos = rank.substring(rank.indexOf("0"), rank.indexOf("0")+2);
		}
		
		// TODO: Handle what to do when neither 0 or 1 are present (special ranks)9
		
		side = rank.substring(rank.indexOf("-")+1);
		
		splitString.add(division);
		splitString.add(pos);
		splitString.add(side);
		
		return splitString;
		
	}
	
	public void incRank() {

		// split the rank string into three parts
		Vector<String> splitString = splitRankString(rank);
		
		String division = splitString.get(0);
		int pos = Integer.parseInt(splitString.get(1));
		String side = splitString.get(2);		
		
		// Now increase the rank
		pos--;
		
		// If pos is <= 0, we have to increase the division
		if(pos <= 0) {
			
			// Go from Jonokuchi to Jonidan
			if(division.equals("JK")) {
				division = "JN";
				pos = 15;
			}
			
			// Go from Jonidan to Sandanme
			else if(division.equals("JN")) {
				division = "SD";
				pos = 15;
			}
			
			// Go from Sandanme to Makushita
			else if(division.equals("SD")) {
				division = "MS";
				pos = 15;
			}
			
			// Go from Makushita to Juryo
			else if(division.equals("MS")) {
				division = "J";
				pos = 15;
			}
			
			// Go from Juryo to Makuuchi
			else if(division.equals("J")) {
				division = "M";
				pos = 15;
			}
			
			// For Makuuchi special rules apply
			// Going from Maegashira 1 to Komusubi does not require anything special yet
			else if(division.equals("M")) {
				division = "K";
				pos = 0;
			}
			
			// For sekiwake, either space has to be available
			// Or you had a very convincing kachikoshi (10-5 or higher) as komusubi
			else if(division.equals("K")) {
				// TODO: Implement
				division = "S";
				pos = 0;
			}
			
			// For Ozeki, the total wins of your last three tournaments has to be
			// at least 33
			else if(division.equals("S")) {
				// TODO: Implement
				division = "O";
				pos = 0;
			}
			
			// For Yokozuna, you have to have at least two back to back tournament wins
			else if(division.equals("O")) {
				// TODO: Implement
				division = "Y";
				pos = 0;
			}			
		}
			
		// TODO:
		// If rank exists on any other rikishi, try switching side from east to west or vice versa
		// If rank still exists on any other rikishi, try one position lower/higher
		
		String strPos = Integer.toString(pos);
		
		// Converting the Integer 9 to String gives us "9", however we want a leading 0
		// for single digit ranks
		if(strPos.length() == 1)
			strPos = "0"+strPos;
		
		// Set the final rank
		// TODO: For Komusubi upwards, drop the "pos"
		rank = division + strPos + "-" + side;
	}
	
	public void decRank() {
		
		// split the rank string into three parts
		Vector<String> splitString = splitRankString(rank);
		
		String division = splitString.get(0);
		int pos = Integer.parseInt(splitString.get(1));
		String side = splitString.get(2);		
		
		// Now decrease the rank
		pos++;
		
		// If pos is > 15, drop down a division
		if(pos > 15) {
			
			// Drop from Jonidan to Jonokuchi
			if(division.equals("JN")) {
				division = "JK";
				pos = 1;
			}
			
			// Drop from Sandanme to Jonidan
			else if(division.equals("SD")) {
				division = "JN";
				pos = 1;
			}
			
			// Drop from Makushita to Sandanme
			else if(division.equals("MS")) {
				division = "SD";
				pos = 1;
			}
			
			// Drop from Juryo to Makushita
			else if(division.equals("J")) {
				division = "MS";
				pos = 1;
			}
			
			// Drop from Makuuchi to Juryo
			else if(division.equals("M")) {
				division = "J";
				pos = 1;
			}
			
			// Drop from komusubi to Maegashira
			else if(division.equals("K")) {
				division = "M";
				pos = 1;
			}
			
			// Drop from Sekiwake to Komusubi
			else if(division.equals("S")) {
				division = "K";
				pos = 0;
			}
			
			// Drop from Ozeki to Sekiwake requires to have two back to back
			// makekoshi
			else if(division.equals("O")) {
				// TODO: Implement
				division = "S";
				pos = 0;
			}
			
			// You cannot drop from Yokozuna, however if your last tournament scores
			// Were lousy, you are forced to retire
			else if(division.equals("Y")) {
				// TODO: Implement
			}			
		}
		
		// TODO:
		// If rank exists on any other rikishi, try switching side from east to west or vice versa
		// If rank still exists on any other rikishi, try one position lower/higher
		
		String strPos = Integer.toString(pos);
		
		// Converting the Integer 9 to String gives us "9", however we want a leading 0
		// for single digit ranks
		if(strPos.length() == 1)
			strPos = "0"+strPos;
		
		// Set the final rank
		// TODO: For Komusubi upwards, drop the "pos"
		rank = division + strPos + "-" + side;	
	}
	
	public void setRank(String rank) {
		this.rank = rank;
	}

	public Heya getHomeHeya() {
		return homeHeya;
	}

}
