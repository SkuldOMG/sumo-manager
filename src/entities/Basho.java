package entities;
import gamestates.GameplayState;

import java.util.Collections;
import java.util.Vector;


public class Basho {
	
	Vector<Rikishi> participants;
	int day;
	
	public Basho(Vector<Rikishi> participants) {
		
		this.participants = participants;
		day = 1;
	}
	
	/**
	 * One tournament day consists of each rikishi fighting once against
	 * a random other rikishi.
	 */
	public void startDay() {
		
		// If overall rikishi is an uneven number, create a new low ranked
		// wrestler to participate
		if(participants.size() % 2 != 0)
			participants.add(new Rikishi(null));
		
		// Shuffle the vector
		Collections.shuffle(participants);
		
		// Let each rikishi fight the next in line
		for(int i = 0; i < participants.size(); i+=2) {			
			
			participants.get(i).fight(participants.get(i+1));		
		}
		
		day++;
	}
	
	/**
	 * A tournament lasts 15 days. On the last day, the winner is
	 * determined.
	 */
	public void calcResults() {
		
		System.out.println("Final results:\n");
		
		// Determine the aftermath
		Rikishi winner = participants.get(0);
		
		for(int i = 0; i < participants.size(); i++) {
			
			// Print results
			System.out.println(participants.get(i).getName()+": "+participants.get(i).getWins()+"-"+participants.get(i).getLosses());
			
			// Determine winner
			if(participants.get(i).getWins() >= winner.getWins())
				winner = participants.get(i);
			
			// Set new ranks
			if(participants.get(i).getWins() > participants.get(i).getLosses())
				participants.get(i).incRank();
			else
				participants.get(i).decRank();			
		}
		
		// Check if we have a victory tie
		// We take the number of wins the overall winner had
		int winnersWins = winner.getWins();
		Vector<Rikishi> tie = new Vector<Rikishi>();
		
		// Now check if any other wrestlers had the same record
		for(int i=0; i < participants.size(); i++) {
			if(participants.get(i).getWins() == winnersWins) {
				tie.add(participants.get(i));
			}
		}
		
		// If there's more than one fighter that has the highest score, we have a tie
		if(tie.size() > 1) {
			
			System.out.println("Ladies and gentlemen, we have a tie between "+tie.get(0).getName()
					+" and "+tie.get(1).getName());
			
			// TODO: Handle events for a three-way or higher tie
			
			// For now, just make the two tied wrestlers fight again
			tie.get(0).fight(tie.get(1));		
			
			// Check again who won
			if(tie.get(0).getWins() > tie.get(1).getWins())
				winner = tie.get(0);
			else
				winner = tie.get(1);
		}
		
		
		System.out.println(winner.getName()+" wins the yusho! Congratulations!\n");
		
		if(winner.getLosses() == 0)
			System.out.println("Zensho yusho!!!\n");
		
		// Handle money prizes if the player's wrestlers won matches (according to rank)
		// Money only starts to roll in when wrestlers have juryo rank
		Vector<Rikishi> myRikishi = new Vector<Rikishi>();
		
		// First find all rikishi that belong to the player
		for(int i=0; i < participants.size(); i++) {
			
			// TODO: Check this for the player's heya name
			if(participants.get(i).getHomeHeya() != null)
				myRikishi.add(participants.get(i));
			
			
		}
		// Check the rikishi's rank
		// TODO: If it's Juryo or higher, grant money for each win
		// All rikishi that rank below that get 500 each regardless of performance
		myRikishi.get(0).getHomeHeya().incMoneyBy(500 * myRikishi.size());
	}
	
	/**
	 * Prints a list of all rikishi with their rank
	 * 
	 * TODO: Make this ordered by rank and maybe nicely formatted.
	 */
	public void printBanzuke() {
		
		for(int i = 0; i < participants.size(); i++) {
			
			System.out.println(participants.get(i).getName()+": "+participants.get(i).getRank());
			
		}
		
		
	}

	public int getDay() {
		return day;
	}
}
