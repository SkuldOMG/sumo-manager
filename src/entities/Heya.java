package entities;
import java.util.Vector;


public class Heya {

	private String name;
	private Vector<Rikishi> rikishiVector;
	private int money;
	private int capacity;	// how many rikishi the heya can currently hold
	
	public Heya(String name) {
		this.name = name;
		rikishiVector = new Vector<Rikishi>();
		money = 1000;
		capacity = 2;
	}
	
	public void addRikishi(Rikishi rikishi) {
		rikishiVector.add(rikishi);
	}
	
	public void removeRikishi(Rikishi rikishi) {
		
		for(int i = 0; i < rikishiVector.size(); i++) {
			
			if(rikishiVector.get(i).getName() == rikishi.getName())
				rikishiVector.remove(i);			
		}
	}
	
	public String getStats() {
		
		String stats = "You have "+rikishiVector.size()+" rikishi.\n";
		
		stats += "Your capacity is currently at "+getCap()+". You have room for "+
				(getCap() - rikishiVector.size())+" more rikishi.";
		
		return stats;
	}
	
	public String getName() {
		return name;
	}
	
	public int getMoney() {
		return money;
	}
	
	public void incMoneyBy(int amount) {
		money += amount;
	}
	
	public void decMoneyBy(int amount) {
		money -= amount;
	}
	
	public int getCap() {
		return capacity;
	}
	
	public void incCapacity() {
		capacity++;
	}
	
	public Vector<Rikishi> getRikishi() {
		return rikishiVector;
	}
}
